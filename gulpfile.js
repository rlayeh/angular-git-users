var gulp = require('gulp');
var browserify = require('browserify');
var reactify = require('reactify');
var source = require('vinyl-source-stream');
var jasmine = require('gulp-jasmine');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var minifyCSS = require('gulp-minify-css');
var rename = require('gulp-rename');

function swallowError (error) {

  // If you want details of the error in the console
  console.log(error.toString());

  this.emit('end');
}
gulp.task('jasmine', function(){
	return gulp.src('spec/**/*.js')
        .pipe(jasmine())
		.on('error', swallowError);
});

gulp.task('browserify', function(){
	var browserified = function(filename) {
		var b = browserify('./src/js/main.js');
		b.transform(reactify);
		return b.bundle();
	};

	var entryFile = './src/js/main.js';
	
	return browserified(entryFile)
				.pipe(source('main.js'))
				.pipe(gulp.dest('./dist/js'))
				.on('error', swallowError);
});

gulp.task('sass', function () {
  gulp.src('./sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('css'))
        .pipe(concat('style.css'))
        .pipe(gulp.dest('./dist/css'))
        .pipe(minifyCSS())
        .pipe(rename('style.min.css'))
        .pipe(gulp.dest('./dist/css'));
		
	gulp.src('./sass/img/**/*')
		.pipe(gulp.dest('dist/css/img'))
		.on('error', swallowError);
})

gulp.task('copy', function(){
	gulp.src('src/index.html')
		.pipe(gulp.dest('dist'));
});

gulp.task('default', ['jasmine', 'browserify', 'sass', 'copy']);

gulp.task('watch', function(){
	gulp.watch('src/**/*.*', ['default']);
	gulp.watch('sass/**/*.*', ['default']);
});

gulp.task('jasmineWatch', function(){
	gulp.watch('src/**/*.*', ['jasmine']);
	gulp.watch('spec/**/*.*', ['jasmine']);
});