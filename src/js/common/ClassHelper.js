var ClassExtender = require('./ClassExtender');

var classHelper = {
	extender: function(){
		return new ClassExtender();
	}
};

module.exports = classHelper;