var EventLimiter = function(callback, interval){	
	this._callback = callback;
	this._interval = interval;
	this._ongoing = false;
	this._invokeAfterDone = false;
};

EventLimiter.prototype = {
	invoke: function(){
		if(this._ongoing){
			this._invokeAfterDone = true;
			return;
		}

		this._execute();
	},
	_scheduleOngoing: function(){
		if(this._ongoing){
			this._invokeAfterDone = true;
			return true;
		}
		return false;
	},
	_execute: function(){
		this._ongoing = true;
		this._triggerCallback();
		this._scheduleExecution();
	},
	_scheduleExecution: function(){
		setTimeout(this._executeDelayed.bind(this),this._interval);
	},
	_executeDelayed: function(){
		if(this._invokeAfterDone){
			this._triggerCallback();
		}
		this._done();
	},
	_done: function(){
		this._ongoing = false;
		this._invokeAfterDone = false;
	},
	_triggerCallback: function(){
		if(this._callback){
			this._callback();
		}
	}
};

module.exports = EventLimiter;