var ClassExtender = function(){
	this._base = null;
	this._inheriting = null;
	this._proto = null;
};

ClassExtender.prototype = {
	base: function(baseClass){
		this._base = baseClass;
		return this;
	},
	ineritting: function(inheriting){
		this._inheriting = inheriting;
		return this;
	},
	proto: function(proto){
		this._proto = proto;
		return this;
	},
	extend: function(){
		this._inheriting.prototype = new this._base();

  		for(var key in this._proto){
	        if(this._proto.hasOwnProperty(key)){
	            this._inheriting.prototype[key] = this._proto[key];
	        }
	    }
	}
};

module.exports = ClassExtender;