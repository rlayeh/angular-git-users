var BaseLoader = function(){
	this._onLoadFinished = null;
	this._onLoadFailed = null;
	this._authenticationToken = null;
};

BaseLoader.prototype = {
	authenticationToken: function(authenticationToken){
		this._authenticationToken = authenticationToken;
		return this;
	},
	onLoadFinished: function(callback){
		this._onLoadFinished = callback;
		return this;
	},
	onLoadFailed: function(callback){
		this._onLoadFailed = callback;
		return this;
	},
	load: function(){
		throw 'should be overridden by an inheriting class';
	}
};

module.exports = BaseLoader;