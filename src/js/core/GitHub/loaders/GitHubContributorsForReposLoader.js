var ClassHelper = require('../../../common/ClassHelper');
var BaseLoader = require('./BaseLoader');
var GitHubUsersStore = require('../stores/GitHubUsersStore');
var GitHubMultiContributorsLoader = require('./GitHubMultiContributorsLoader');
var GitHubUsersLoader = require('./GitHubUsersLoader');

var GitHubContributorsForReposLoader = function(repositoriesStore, usersStore) {
    if (!repositoriesStore) {
        throw 'repositories store is required';
    }

    this._usersStore = usersStore || new GitHubUsersStore();
    this._repositoriesStore = repositoriesStore;
    this._onContributorsLoaded = null;
};

ClassHelper.extender()
    .base(BaseLoader)
    .ineritting(GitHubContributorsForReposLoader)
    .proto({
        onContributorsLoaded: function(callback) {
            this._onContributorsLoaded = callback;
            return this;
        },
        load: function() {
            this._loadRepositoriesContributors();
        },
        _loadRepositoriesContributors: function() {
            var reposNames = this._repositoriesStore.getRepositoriesNames();
            new GitHubMultiContributorsLoader(reposNames, this._usersStore)
                .authenticationToken(this._authenticationToken)
                .onLoadFinished(this._loadUsersData.bind(this))
                .onLoadFailed(this._onLoadFailed)
                .load();

        },
        _loadUsersData: function() {
            this._triggerOnContributorsLoaded();

            new GitHubUsersLoader(this._usersStore)
                .authenticationToken(this._authenticationToken)
                .onLoadFinished(this._loadFinished.bind(this))
                .onLoadFailed(this._onLoadFailed)
                .load();
        },
        _loadFinished: function() {
            if (this._onLoadFinished) {
                this._onLoadFinished(this._usersStore, this._repositoriesStore);
            }
        },
        _triggerOnContributorsLoaded: function() {
            if (this._onContributorsLoaded) {
                this._onContributorsLoaded(this._usersStore);
            }
        }
    }).extend();

module.exports = GitHubContributorsForReposLoader;
