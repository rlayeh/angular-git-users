var ClassHelper = require('../../../common/ClassHelper');
var BaseLoader = require('./BaseLoader');
var GithubApiRequest = require('../../../net/GithubApiRequest');

var PAGE_SIZE = 100;

var GitHubContributorsLoader = function(repositoryName) {
    if (!repositoryName) {
        throw 'repository name is required';
    }

    this._repositoryName = repositoryName;
    this._users = [];
};

ClassHelper.extender()
    .base(BaseLoader)
    .ineritting(GitHubContributorsLoader)
    .proto({
        load: function() {
            this._fetchNextPage(1);
        },
        _fetchNextPage: function(pageNumber) {
            new GithubApiRequest()
                .async(true)
                .pageSize(PAGE_SIZE)
                .pageNumber(pageNumber)
                .onSuccess(this._pageFetched.bind(this, pageNumber))
                .onFailed(this._onLoadFailed)
                .authenticationToken(this._authenticationToken)
                .getContributors(this._repositoryName);
        },
        _pageFetched: function(pageNumber, result) {
            if (result && result.length) {
                this._users = this._users.concat(result);
            }

            if (!result || !result.length || PAGE_SIZE > result.length) {
                this._onLoadFinished(this._users);
                return;
            }

            this._fetchNextPage(pageNumber + 1);
        }
    }).extend();

module.exports = GitHubContributorsLoader;
