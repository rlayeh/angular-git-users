var ClassHelper = require('../../../common/ClassHelper');
var BaseLoader = require('./BaseLoader');
var GithubApiRequest = require('../../../net/GithubApiRequest');

var GitHubUsersLoader = function(usersStore) {
    if (!usersStore) {
        throw 'users store is required';
    }

    this._usersStore = usersStore;
    this._loadedCount = 0;
};

ClassHelper.extender()
    .base(BaseLoader)
    .ineritting(GitHubUsersLoader)
    .proto({
        load: function() {
            for (var i = 0, user = null; i < this._usersStore.getTotalCount(); i++) {
                user = this._usersStore.getByIndex(i);
                this._fetchUser(user.get('login'));
            }
        },
        _fetchAsync: function(userName){
            setTimeout(this._fetchUser.bind(this, userName),0);
        },
        _fetchUser: function(userName) {
            new GithubApiRequest()
                .async(true)
                .onSuccess(this._onUserFetched.bind(this))
                .onFailed(this._onLoadFailed)
                .authenticationToken(this._authenticationToken)
                .getUser(userName);
        },
        _onUserFetched: function(result) {
            this._loadedCount++;
            this._usersStore.updateUser(result);
            this._checkIfDone();
        },
        _checkIfDone: function() {
            if (this._loadedCount === this._usersStore.getTotalCount()) {
                this._onLoadFinished(this._usersStore);
            }
        }
    }).extend();

module.exports = GitHubUsersLoader;
