var ClassHelper = require('../../../common/ClassHelper');
var BaseLoader = require('./BaseLoader');
var GithubApiRequest = require('../../../net/GithubApiRequest');
var GitHubRepositoriesStore = require('../stores/GitHubRepositoriesStore');

var PAGE_SIZE = 100;
var GitHubRepositoriesLoader = function(mainUserName, repositoriesStore) {
    if (!mainUserName) {
        throw 'main user name is required';
    }

    this._mainUserName = mainUserName;
    this._repositoryStore = repositoriesStore || new GitHubRepositoriesStore();
};

ClassHelper.extender()
    .base(BaseLoader)
    .ineritting(GitHubRepositoriesLoader)
    .proto({
        load: function() {
            this._fetchNextPage(1);
        },
        _fetchNextPage: function(pageNumber) {
            new GithubApiRequest()
                .async(true)
                .pageSize(PAGE_SIZE)
                .pageNumber(pageNumber)
                .onSuccess(this._pageFetched.bind(this, pageNumber))
                .onFailed(this._onLoadFailed)
                .authenticationToken(this._authenticationToken)
                .getRepositories(this._mainUserName);
        },
        _pageFetched: function(pageNumber, result) {
            if (result && result.length) {
                this._repositoryStore.addRepositories(result);
            }

            if (!result || !result.length || PAGE_SIZE > result.length) {
                this._onLoadFinished(this._repositoryStore);
                return;
            }

            this._fetchNextPage(pageNumber + 1);
        }
    }).extend();

module.exports = GitHubRepositoriesLoader;
