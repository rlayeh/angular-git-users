var ClassHelper = require('../../../common/ClassHelper');
var BaseLoader = require('./BaseLoader');
var GitHubContributorsLoader = require('./GitHubContributorsLoader');
var GitHubUsersStore = require('../stores/GitHubUsersStore');

var GitHubMultiContributorsLoader = function(repositoriesNamesArray, usersStore) {
    if (!repositoriesNamesArray) {
        throw 'repository names list is required';
    }

    this._repositoriesNamesArray = repositoriesNamesArray;
    this._usersStore = usersStore || new GitHubUsersStore();
    this._loadedCount = 0;
};

ClassHelper.extender()
    .base(BaseLoader)
    .ineritting(GitHubMultiContributorsLoader)
    .proto({
        load: function() {
            this._fetchAllContributors();
        },
        _fetchAllContributors: function() {
            for (var i = 0; i < this._repositoriesNamesArray.length; i++) {
                this._fetchForRepository(this._repositoriesNamesArray[i]);
            }
        },
        _fetchForRepository: function(repositoryName) {
            new GitHubContributorsLoader(repositoryName)
                .authenticationToken(this._authenticationToken)
                .onLoadFinished(this._loadedForRepository.bind(this, repositoryName))
                .onLoadFailed(this._onLoadFailed)
                .load();
        },
        _loadedForRepository: function(repositoryName, result) {
            this._usersStore.addUsers(result, repositoryName);
            this._loadedCount++;
            this._checkIfDone();
        },
        _checkIfDone: function() {
            if (this._loadedCount === this._repositoriesNamesArray.length) {
                this._onLoadFinished(this._usersStore);
            }
        }

    }).extend();

module.exports = GitHubMultiContributorsLoader;
