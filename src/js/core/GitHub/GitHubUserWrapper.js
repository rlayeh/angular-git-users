var GitHubUserWrapper = function(userObj){
	this._userObject = userObj || {};
	this._repositories = this._userObject.repository ? [this._userObject.repository] : [];
	this._totalContributions = this._userObject.contributions || 0;
};

GitHubUserWrapper.deserialize = function(genericObject){
	return new GitHubUserWrapper().deserialzie(genericObject);
}

GitHubUserWrapper.prototype = {
	getUserData: function(){
		this._userObject.contributions = this._totalContributions;
		return this._userObject;
	},
	get: function(fieldName){
		if(fieldName === 'contributions'){
			return this._totalContributions;
		}
		return this._userObject[fieldName];
	},
	getRepositories: function(){
		return this._repositories;
	},
	addRepository: function(repositoryId){
		this._repositories.push(repositoryId);
	},
	updateContributionsCount: function(count){
		this._totalContributions += (count || 0);
	},
	updateUserData: function(newUserData){
	    for(var key in newUserData){
	        if(newUserData.hasOwnProperty(key)){
	            this._userObject[key] = newUserData[key];
	        }
	    }
	},
	serialize: function(){
		return {
			user: this._userObject,
			repsoitories: this._repositories,
			contributions: this._totalContributions
		}
	},
	deserialzie: function(genericObject){
		if(genericObject){
			this._userObject = genericObject.user || {};
			this._repositories = genericObject.repsoitories || [];
			this._totalContributions = genericObject.contributions || 0;
		}

		return this;
	}
};

module.exports = GitHubUserWrapper;