var ClassHelper = require('../../../common/ClassHelper');
var BaseStore = require('./BaseStore');
var GitHubRepositoriesLoader = require('../loaders/GitHubRepositoriesLoader');

var GitHubRepositoriesStore = function(repositories) {
    this._repositories = repositories || [];
};

ClassHelper.extender()
    .base(BaseStore)
    .ineritting(GitHubRepositoriesStore)
    .proto({
    	getLoader: function(userName){
    		return new GitHubRepositoriesLoader(userName, this);
    	},
        setData: function(data) {
            this._repositories = data;
            return this;
        },
        getData: function() {
            return this._repositories;
        },
        addRepository: function(repository) {
            this._repositories.push(repository);
        },
        addRepositories: function(repositoriesArray) {
            this._repositories = this._repositories.concat(repositoriesArray);
        },
        getRepositoriesNames: function() {
            return this._repositories.map(function(repository) {
                return repository.full_name;
            });
        },
        getRepositoryByName: function(repositoryName) {
            for (var i = 0; i < this._repositories.length; i++) {
                var repo = this._repositories[i];
                if (repo.full_name === repositoryName) {
                    return repo;
                }
            }

            return null;
        }
    }).extend();

module.exports = GitHubRepositoriesStore;
