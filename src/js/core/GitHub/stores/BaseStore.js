var CacheManager = require('../../CacheManager');

var BaseStore = function(){
	this._onLoadFinished = null;
	this._onLoadFailed = null;
	this._authenticationToken = null;
	this._id = null;
};

BaseStore.prototype = {
	id: function(id){
		this._id = id;
		return this;
	},
	authenticationToken: function(authenticationToken){
		this._authenticationToken = authenticationToken;
		return this;
	},
	onLoadFinished: function(callback){
		this._onLoadFinished = callback;
		return this;
	},
	onLoadFailed: function(callback){
		this._onLoadFailed = callback;
		return this;
	},
	setData: function(){
		throw 'should be overridden by an inheriting class';
	},
	getData: function(){
		throw 'should be overridden by an inheriting class';
	},
	load: function(){
		if(this._loadFromCache()){
			return;
		}

		this.getLoader.apply(this, arguments)
		.authenticationToken(this._authenticationToken)
		.onLoadFinished(this._loadFinished.bind(this))
		.onLoadFailed(this._onLoadFailed)
		.load();
	},
	getLoader: function(){
		throw 'should be overridden by an inheriting class';
	},
	_loadFinished: function(){
		this._addToCache();
		this._triggerOnLoadFinished();
	},
	_loadFromCache: function(){
		var valueFromCache = this._takeFromCache();
		if(valueFromCache){
			this.setData(valueFromCache);
			this._triggerOnLoadFinished();
			return true;
		}
		return false;
	},
	_addToCache:function(){
		if(this._id){
			CacheManager.set(this._id, this.getData());
		}
	},
	_takeFromCache: function(){
		if(this._id && !CacheManager.isCacheOutdated(this._id)){
			return CacheManager.get(this._id);
		}

		return null;
	},
	_triggerOnLoadFinished: function(){
		if(this._onLoadFinished){
			this._onLoadFinished();
		}
	}
};

module.exports = BaseStore;