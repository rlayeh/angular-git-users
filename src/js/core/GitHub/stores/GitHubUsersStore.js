var ClassHelper = require('../../../common/ClassHelper');
var BaseStore = require('./BaseStore');
var GitHubUserWrapper = require('../GitHubUserWrapper');
var FilterExpression = require('../../FilterExpression');
var comparisonOperators = require('../../ComparisonOperators')
var GitHubContributorsForReposLoader = require('../loaders/GitHubContributorsForReposLoader');


var GitHubUsersStore = function(users) {
    this._users = users || [];
    this._filteredUsers = this._users; //for performance reasons filteredUsers are cached. We need to refresh when new users are added.
    this._filterExpression = null;
    this._onChange = null;
    this._onUsersLoaded = null;
};

ClassHelper.extender()
    .base(BaseStore)
    .ineritting(GitHubUsersStore)
    .proto({
        getLoader: function(repositoriesStore){
            return new GitHubContributorsForReposLoader(repositoriesStore, this)
                        .onContributorsLoaded(this._triggerOnUsersLoaded.bind(this));
        },
        setData: function(data) {
            this._users = data.map(function(dataEntry){
                return GitHubUserWrapper.deserialize(dataEntry);
            });
            this._setFilteredUsers();
            this._triggerOnChange();
            return this;
        },
        getData: function() {
            return this._users.map(function(user){
                return user.serialize();
            });
        },
        onChange: function(callback) {
            this._onChange = callback;
            return this;
        },
        onUsersLoaded: function(callback){
            this._onUsersLoaded = callback;
            return this;
        },
        addUsers: function(usersAsObjectsArray, repository) {
            for (var i = 0; i < usersAsObjectsArray.length; i++) {
                this._addUser(usersAsObjectsArray[i], repository);
            }
            this._setFilteredUsers();
            this._triggerOnChange();
            return this;
        },
        updateUser: function(userAsObject) {
            var user = this.getByUserName(userAsObject.login);
            if (!user) {
                throw 'user was not found in the store';
            }

            user.updateUserData(userAsObject);
            this._triggerOnChange();
            return this;
        },
        getTotalCount: function() {
            return this._filteredUsers ? this._filteredUsers.length : 0;
        },
        getByIndex: function(index) {
            if (this._filteredUsers &&
                this._filteredUsers.length &&
                this._filteredUsers.length > index) {

                return this._filteredUsers[index];
            }

            return null;
        },
        getUsers: function(){
            return this._filteredUsers;
        },
        getByUserName: function(userName) {
            if (!userName) {
                throw 'user name is not specified';
            }

            return this._filteredUsers ? this._getUser(userName) : null;
        },
        filter: function(filterExpression) {
            this._filterExpression = filterExpression;
            this._setFilteredUsers();
            return this;
        },
        getFilter: function(){
            return this._filterExpression;
        },
        _setFilteredUsers: function() {
            if (!this._filterExpression) {
                this._filteredUsers = this._users;
                return;
            }
            var filterHandler = this._checkFilterCriteria.bind(this);

            this._filteredUsers = this._users.filter(filterHandler);
        },
        _checkFilterCriteria: function(user) {
            var userValue = user.get(this._filterExpression.getField());
            var filterValue = this._filterExpression.getValue();
            var operator = this._filterExpression.getOperator();

            return this._compareValues(userValue, filterValue, operator);
        },
        _compareValues: function(value1, value2, operator) {
            return (comparisonOperators.greaterThan == operator && value1 > value2) ||
                (comparisonOperators.lessThan == operator && value1 < value2) ||
                (comparisonOperators.equalTo == operator && value1 == value2); //abstract equality operator used on purpose. It is possible that we will compare 1 against '1'
        },
        _addUser: function(userAsObject, repository) {
            if (!this._updateExisting(userAsObject, repository)) {
                this._addNewUser(userAsObject, repository);
            }
        },
        _getUser: function(login) {
            for (var i = 0; i < this._filteredUsers.length; i++) {
                var user = this._filteredUsers[i];
                if (user.get('login') === login) {
                    return user;
                }
            }
            return null;
        },
        _updateExisting: function(userData, repository) {
            var user = this.getByUserName(userData.login);

            if (user) {
                user.updateContributionsCount(userData.contributions);
                this._addRepositoryToTheUser(user, repository);

                return true;
            }
            return false;
        },
        _addNewUser: function(userData, repository) {
            var newUser = new GitHubUserWrapper(userData);
            this._addRepositoryToTheUser(newUser, repository);

            this._users.push(newUser);
        },
        _triggerOnChange: function() {
            if (this._onChange) {
                this._onChange();
            }
        },
        _triggerOnUsersLoaded: function(){
            if(this._onUsersLoaded){
                this._onUsersLoaded();
            }
        },
        _addRepositoryToTheUser: function(user, repository) {
            if (repository) {
                user.addRepository(repository);
            }
        }
    }).extend();

module.exports = GitHubUsersStore;
