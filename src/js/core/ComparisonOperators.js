var operators = {
	equalTo: 1,
	greaterThan: 2,
	lessThan: 3
};

module.exports = operators;