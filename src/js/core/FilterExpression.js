var comparisonOperators = require('./ComparisonOperators')

var FilterExpression = function(field, value, operator){
	this._field = field;
	this._value = value;
	this._operator = operator || comparisonOperators.equalTo;
};

FilterExpression.prototype = {
	field: function(field){
		this._field = field;
		return this;
	},
	getField: function(){
		return this._field;
	},
	value: function(value){
		this._value = value;
		return this;
	},
	getValue: function(){
		return this._value;
	},
	operator: function(operator){
		this._operator = operator;
		return this;
	},
	getOperator: function(){
		return this._operator;
	}
};

module.exports = FilterExpression;