var comparisonOperators = require('./ComparisonOperators')

var CacheManager = function(){
};

CacheManager.prototype = {
	get: function(entryId){
		var storageObject = this._getFromLocalStorage(entryId);
		return storageObject ? storageObject.value : null;
	},
	isCacheOutdated: function(entryId){
		var storageObject = this._getFromLocalStorage(entryId);

		return !storageObject || 
			   !storageObject.modificationDate || 
			   this._checkIfOutdated(storageObject.modificationDate);
	},
	set: function(entryId, dataEntry){
		if(localStorage){
			localStorage.removeItem(entryId);  

			var entry = {
				value: dataEntry,
				modificationDate: new Date()
			};

        	var dataAsString = JSON.stringify(entry);
            localStorage.setItem(entryId,dataAsString);
		}
	},
	_getFromLocalStorage: function(entryId){
		var entry = localStorage ? localStorage.getItem(entryId) :  null;
		if(entry){
			return JSON.parse(entry);
		}

		return null;
	},
	_checkIfOutdated: function(modificationDate){
		var currDate = new Date();
		currDate.setHours(currDate.getHours()-1);

		return currDate > new Date(modificationDate);
	}
};

module.exports = new CacheManager();