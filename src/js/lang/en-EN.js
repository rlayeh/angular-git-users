var langDef = {
	loader: {
		slide1: 'GitHub is all about the people',
		slide2: 'In around one year from creation, GitHub already had over 100 000 users.',
		slide3: 'Currently there are over 3 000 000 users on GitHub!',
		slide4: 'Users are making GitHub special.',
		loading: 'loading..'
	},
	enums:{
		comparisonOperators:{
			equalTo: "=",
			greaterThan: ">",
			lessThan: "<"
		}
	},
	global: {
		anonymous: 'Anonymous'
	},
	user:{
		totalContributions: 'Total contributions',
		details:{
			login: "Login",
			name: "Name",
			id: "ID",
			html_url: "GitHub url",
			followers_url: "Followers",
			following_url: "Following",
			gists_url: "Gists",
			starred_url: "Starred",
			subscriptions_url: "Subscriptions",
			organizations_url: "Organizations",
			repos_url: "Repos",
			events_url: "Events",
			received_events_url: "Received events",
			type: "Type",
			site_admin: "Is Admin",
			company: "Company",
			blog: "Blog",
			location: "Location",
			email: "E-mail",
			hireable: "Is hireable:",
			public_repos: "Repos",
			public_gists: "Gists",
			followers: "Followers",
			following: "Following",
			contributions: "Contributions"
		}
	},
	details: {
		navigateBack: '<< Back to search results'
	},
	loaderBar:{
		loading: 'Loading..'
	},
	reposList: {
		repos: 'Repositories:'
	},
	baner: {
		gitHub: 'GitHub',
		community: "Angular community"
	}
};


module.exports = langDef;