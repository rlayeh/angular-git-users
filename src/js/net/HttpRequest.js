var UrlCreator = require('./UrlCreator');
var nanoajax = require('nanoajax');

var HttpRequest = function(path){
	this._urlCreator = new UrlCreator(path);
	this._onSuccess = null;
	this._onFailed = null;
	this._method = 'GET';
	this._body = null;
	this._headers = null;
	this._responseAsObject = false;
	this._async = false;
};

HttpRequest.prototype = {
	async: function(async){
		this._async = async;
		return this;
	},
	appendUrl: function(pathPart){
		this._urlCreator.append(pathPart);
		return this;
	},
	appendAttribute: function(attributeName, value){
		this._urlCreator.appendAttribute(attributeName, value);
		return this;
	},
	onSuccess: function(callback){
		this._onSuccess = callback;
		return this;
	},
	onFailed: function(callback){
		this._onFailed = callback;
		return this;
	},
	method: function(method){
		this._method = method;
		return this;
	},
	body: function(body){
		this._body = body;
		return this;
	},
	headers: function(headers){
		this._headers = headers;
		return this;
	},
	responseAsObject: function(responseAsObject){
		this._responseAsObject = responseAsObject;
		return this;
	},
	execute: function(){
		var config = this._getConfig();
		this._executeAjax(config);
	},
	_getConfig: function(){
		var path = this._urlCreator.create();
		if(!path){
			throw "Path is not specified";
 		}

 		return {
 			url: path,
 			method: this._method,
 			body: this._getBody(),
 			headers: this._headers
 		};
	},
	_getBody: function(){
		if(!this._body){
			return null;
		}

		var bodyParts = [];

		for(var bodyPart in this._body){
			bodyParts.push(bodyPart + '=' + this._body[bodyPart]); //naive conversion
		}
		return bodyParts.join('&');
	},
	_executeAjax: function(config){
		var responseHandler = this._response.bind(this);

		if(!this._async){
			nanoajax.ajax(config, responseHandler);
			return;
		}
		
		//trigger in async mode
		this._executeAsync(config, responseHandler);
	},	
	_executeAsync: function(config, handler){
		setTimeout(function(){
			nanoajax.ajax(config, handler);
		},0);
	},
	_response: function(code, responseText, request){
		var newResponse = responseText;
		if(this._isSuccesfull(code)){
			this._fireOnSuccess(this._getConvertedResponse(responseText));
			return;
		}
		this._fireOnFailed(code, responseText, request);
	},
	_getConvertedResponse: function(responseText){
		return this._responseAsObject && responseText ? JSON.parse(responseText) : responseText;
	},
	_isSuccesfull: function(code){
		return code >= 200 && code < 300;
	},
	_fireOnSuccess: function(response){
		if(this._onSuccess){
			this._onSuccess(response);
		}
	},
	_fireOnFailed: function(code, responseText, request){
		if(this._onFailed){
			this._onFailed(code, responseText, request);
		}
	}
};

module.exports = HttpRequest;