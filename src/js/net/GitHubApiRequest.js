var HttpRequest = require('./HttpRequest');

var GITHUB_URL = "https://api.github.com";


var GitHubApiRequest = function(){
	this._authenticationToken = null;
	this._onSuccess = null;
	this._onFailed = null;
	this._pageSize = 0;
	this._pageNumber = 0;
	this._async = false;
};

GitHubApiRequest.prototype = {
	async: function(async){
		this._async = async;
		return this;
	},
	authenticationToken: function(authenticationToken){
		this._authenticationToken = authenticationToken;
		return this;
	},
	onSuccess: function(callback){
		this._onSuccess = callback;
		return this;
	},
	onFailed: function(callback){
		this._onFailed = callback;
		return this;
	},
	pageSize: function(pageSize){
		this._pageSize = pageSize;
		return this;
	},
	pageNumber: function(pageNumber){
		this._pageNumber = pageNumber;
		return this;
	},
	getContributors: function(repositoryName){
		if(!this._checkIfFullName(repositoryName)){
			throw 'repository should be specified by its full name';
		}

		this.getRequest()
			.appendUrl('repos')
			.appendUrl(repositoryName)
			.appendUrl('contributors')
			.execute();
	},
	getUser: function(userName){
		if(!userName){
			throw 'user name is not specified';
		}

		this.getRequest()
			.appendUrl('users')
			.appendUrl(userName)
			.execute();
	},
	getRepositories: function(userName){
		this._attributePresent(userName, 'user name');

		this.getRequest()
			.appendUrl('users')
			.appendUrl(userName)
			.appendUrl('repos')
			.execute();
	},
	getRateLimit: function(){
		this.getRequest()
			.appendUrl('rate_limit')
			.execute();
	},
	getRequest: function(){
		var request = new HttpRequest(GITHUB_URL)
						   .responseAsObject(true)
						   .async(this._async);

		this._extendWithHeaders(request)
			._extendWithHandlers(request)
			._extendWithPaging(request);
		
		return request;
	},
	_attributePresent: function(attribute, attributeName){
		if(!attribute){
			throw attributeName + ' is not specified';
		}
	},
	_checkIfFullName: function(repositoryName){
		return repositoryName && repositoryName.indexOf('/') !== -1;
	},
	_extendWithHeaders: function(request){
		var headers = this._getHeaders();
		if(headers){
			request.headers(headers);
		}
		return this;
	},
	_extendWithHandlers: function(request){
		request.onSuccess(this._onSuccess);
		request.onFailed(this._onFailed);
		return this;
	},
	_getHeaders: function(){
		var headersObj = null;
		headersObj = this._extendWithAuthenticationHeader(headersObj);

		return headersObj;
	},
	_extendWithAuthenticationHeader: function(headersObj){
		var newHeaders = headersObj;
		if(this._authenticationToken){
			newHeaders = newHeaders || {};
			newHeaders.Authorization = 'token ' + this._authenticationToken;
		}
		return newHeaders;
	},
	_extendWithPaging: function(request){
		if(this._pageSize){
			request.appendAttribute('per_page', this._pageSize);
		}
		if(this._pageNumber){
			request.appendAttribute('page', this._pageNumber);
		}
		return this;
	}
};

module.exports = GitHubApiRequest;