var UrlCreator = function(basePath){
	this._pathParts = basePath ? [basePath] : [];
	this._separator = '/';
	this._pathAttributes = [];
};

UrlCreator.prototype = {
	append: function(pathPart){
		this._pathParts.push(pathPart);
		return this;
	},
	appendAttribute: function(attributeName, value){
		this._pathAttributes.push(attributeName + '=' + value);
		return this;
	},
	separator: function(separator){
		this._separator = separator;
		return this;
	},
	create: function(){
		var url = this._pathParts.join(this._separator);

		if(this._pathAttributes.length){
			url += "?" + this._pathAttributes.join('&');
		}

		return url
	}
};

module.exports = UrlCreator;