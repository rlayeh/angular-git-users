var React = require('React');
var UsersList = require('./controls/UsersList');
var FilterControl = require('./controls/FilterControl');
var LoaderBar = require('./controls/LoaderBar');
var Banner = require('./controls/Banner');

var MainPage = React.createClass({
	getInitialState: function(){
		return{
			loaded: false
		}
	},
	render: function(){
		return <div className={'scrollable-container'} >
					{this._getControls()}
				</div>;
	},
	_getControls: function(){
		var controls = [];
		var loaded = this.props.loaded;

		controls.push(this._getBanerControl());

		if(!loaded){
			controls.push(this._getLoaderControl());
		}

		controls.push(this._getFilterControl(loaded));
		controls.push(this._getUsersList());

		return controls;
	},
	_getBanerControl: function(){
		return <Banner key={'baner'} />;
	},
	_getLoaderControl: function(){
		return <LoaderBar key={'loader'} />;
	},
	_getFilterControl: function(loaded){
		
		return <FilterControl key={'filter'} filter={this._getFilter()} 
								   id={'filterControl'} 
								   fields={this._getFilterableFields()}
								   disabled={!loaded} />;
	},
	_getUsersList: function(){
		return <UsersList key={'users'} id={'usersList'} 
								singleElementHeight={250}
								singleElementWidth={250}
								getByIndexCallback={this.props.getByIndexCallback} 
								getTotalCountCallback={this.props.getTotalCountCallback} />
	},
	_getFilterableFields: function(){
		return ['contributions', 'followers', 'public_repos', 'public_gists' ];
	},
	_getFilter: function(){
		return this.props.filter;
	}
});

module.exports = MainPage;