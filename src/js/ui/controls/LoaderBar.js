var React = require('React');
var lang = require('../../lang/en-EN');

var LoaderBar = React.createClass({
	render: function(){
		return <div className={'loading-bar'}>
					<span>{lang.loaderBar.loading}</span>
					<div className={'gradient'} ></div>
				</div>;
	}
});

module.exports = LoaderBar;