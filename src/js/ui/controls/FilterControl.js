var React = require('React');
var lang = require('../../lang/en-EN');
var comparisonOperators = require('../../core/ComparisonOperators');
var ActionDispatcher = require('../ActionDispatcher');

var FilterControl = React.createClass({
	render: function(){
		var currentFilter = this._getCurrentFilter();
		return <div className={'filter'} >
					{this._createFieldsOption(currentFilter.getField())}
					{this._createComparisonOperatorsOption(currentFilter.getOperator())}
					{this._createInput(currentFilter.getValue())}
					<div className={this._isDisabled() ? 'maskPanel' : 'maskPanel hidden'}></div>
			   </div>;
	},
	_createFieldsOption: function(currField){
		var fields = this._getFilterableFields();

		fields = fields.map(function(field){
			var fieldLabel = lang.user.details[field];
			return <option  key={field} value={field}>{fieldLabel}</option>;
		});

		return <select onChange={this._fieldChanged} 
					   defaultValue={currField} 
					   className={'fields'} >
				   {fields}
			   </select>
	},
	_createComparisonOperatorsOption: function(currOperator){
		var operators = [];

		for(var operator in comparisonOperators){
			var comparisonOperator = comparisonOperators[operator];
			var operatorLabel = lang.enums.comparisonOperators[operator];

			var operatorOption = <option key={operator} 
										 value={comparisonOperator}>
									 {operatorLabel}
								 </option>;

			operators.push(operatorOption);
		}

		return <select onChange={this._operatorChanged} 
					   defaultValue={currOperator} 
					   className={'operators'}>
				    {operators}
			    </select>;
	},
	_createInput: function(currValue){
		return <input onChange={this._valueChanged} 
					  min={0} 
					  type={"number"} 
					  value={currValue}>
			  	</input>;
	},
	_getFilterableFields: function(){
		return this.props.fields;
	},
	_isDisabled: function(){
		return this.props.disabled;
	},
	_fieldChanged: function(e){
		var value = e.target.value;
		var filter = this._getCurrentFilter()
							.field(value);
							
		this._filterChanged(filter);
	},
	_operatorChanged: function(e){
		var value = e.target.value;
		var filter = this._getCurrentFilter()
							.operator(value);
							
		this._filterChanged(filter);
	},
	_valueChanged: function(e){
		var value = e.target.value;
		var filter = this._getCurrentFilter()
							.value(value);

		this._filterChanged(filter);
	},
	_filterChanged: function(filterExpression){
		ActionDispatcher.trigger('filter', filterExpression);
	},
	_getCurrentFilter: function(){
		return this.props.filter;
	}

});

module.exports = FilterControl;