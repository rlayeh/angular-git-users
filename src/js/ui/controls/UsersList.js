var React = require('React');
var UsersRow = require('./UsersRow');

var UsersList = React.createClass({
	getInitialState: function(){
		return {
			visibleRange: {},
			usersPerRow: 0
		}
	},
	componentDidMount: function(){
		this._addWindowListeners();
		this._calculate();
	},
	componentWillUnmount: function(){
		this._removeWindowListeners();
	},
	render: function(){
		var totalCount = this._getTotalCount();
		var rowsCount = this._calculateRows(this.state.usersPerRow, totalCount);

		var mainDivStyle = {
			height: this._calculateTotalHeight(rowsCount) || '100%'
		};

		var rows = this._getRows(this.state.visibleRange, rowsCount, totalCount, this.state.usersPerRow);

		return <div className={'users-list'} style={mainDivStyle}  >
					{rows}
				</div>;
	},
	_addWindowListeners: function(){
		var domNode = this.getDOMNode();
		var parentNode = domNode.parentNode;
		window.addEventListener('resize', this._windowResized);
		parentNode.addEventListener('scroll', this._scroll);
	},
	_removeWindowListeners: function(){
		var domNode = this.getDOMNode();
		var parentNode = domNode.parentNode;
		window.removeEventListener('resize', this._windowResized);
		parentNode.removeEventListener('scroll', this._scroll);
	},
	_calculate: function(){
		var domNode = this.getDOMNode();
		var visibleRangeInPx = this._getVisibleRangeInPx(domNode);
		var visibleRange = this._getVisibleRange(domNode);
		var usersPerRow = this._calculateUsersPerRow(domNode.clientWidth);

		this.setState({
			visibleRange: visibleRange,
			usersPerRow: usersPerRow
		});
	},
	_scroll: function(){
		var domNode = this.getDOMNode();
		var visibleRangeInPx = this._getVisibleRangeInPx(domNode);
		var visibleRange = this._getVisibleRange(domNode);

		if(this.state.visibleRange.start !== visibleRange.start ||
		   this.state.visibleRange.end !== visibleRange.end){

			this.setState({
				visibleRange: visibleRange
			});
		}
	},
	_windowResized: function(){
		var domNode = this.getDOMNode();
		var usersPerRow = this._calculateUsersPerRow(domNode.clientWidth);

		if(this.state.usersPerRow !== usersPerRow){
			this.setState({
				usersPerRow: usersPerRow
			});
		}
	},

	_getRows: function(visibleRange, rowsCount, totalCount, usersPerRow){
		var singleElementHeight = this._getSingleElementHeight();
		var startingRow = visibleRange.start;
		var endingRow = visibleRange.end > rowsCount ? rowsCount : visibleRange.end;

		var rows = [];

		for(var i=startingRow;i<endingRow;i++){
			rows.push(this._getRow(i, totalCount, usersPerRow));
		}

		return rows;
	},
	_getRow: function(rowNumber, totalCount, usersPerRow){
		var singleElementHeight = this._getSingleElementHeight();
		var singleElementWidth = this._getSingleElementWidth();
		var singleElementSize = {
			height: singleElementHeight,
			width: singleElementWidth
		};

		var rowTop = singleElementHeight * rowNumber;
		var users = this._getUsersForRow(rowNumber, totalCount, usersPerRow);

		return <UsersRow key={rowNumber} users={users} top={rowTop} singleElementSize={singleElementSize} />
	},
	_getUsersForRow: function(rowNumber, totalCount, usersPerRow){
		var start = rowNumber * usersPerRow;
		var totalCountMod = totalCount % usersPerRow;
		var end = start + ((start + totalCountMod) === totalCount ? totalCountMod : usersPerRow);

		var users = [];

		for(var i=start;i<end;i++){
			users.push(this._getByIndex(i));
		}

		return users;
	},
	_getByIndex: function(index){
		return this.props.getByIndexCallback(index);
	},
	_getTotalCount: function(){
		return this.props.getTotalCountCallback();
	},
	_getVisibleRangeInPx: function(domNode){
		var visibleRect = domNode.getBoundingClientRect();
		var parentHeight = domNode.parentNode.clientHeight;

		var top = 0;
		var bottom = 0;
		if(parentHeight > visibleRect.top && visibleRect.bottom > 0){
			var bottomModifier = visibleRect.top > 0 ? visibleRect.top : 0;
			top = Math.min(visibleRect.top, 0) * -1;
			bottom = top + Math.min(parentHeight, visibleRect.bottom) - bottomModifier;
		}

		return {
			top: top,
			bottom: bottom
		};
	},
	_getVisibleRange: function(domNode){
		var visibleRangeInPx = this._getVisibleRangeInPx(domNode);

		var singleElementHeight = this._getSingleElementHeight();

		var height = visibleRangeInPx.bottom - visibleRangeInPx.top;

		var startRow = Math.floor(visibleRangeInPx.top / singleElementHeight);
		var endRow = startRow + Math.ceil(height / singleElementHeight) + 1;

		return {
			start: startRow,
			end: endRow
		};
	},
	_getSingleElementWidth: function(){
		return this.props.singleElementWidth || 50; //50 is default
	},
	_getSingleElementHeight: function(){
		return this.props.singleElementHeight || 50; //50 is default
	},
	_calculateUsersPerRow: function(width){
		var singleElementWidth = this._getSingleElementWidth();

		return Math.floor(width/singleElementWidth);
	},
	_calculateRows: function(usersPerRow, totalCount){
		return Math.ceil(totalCount/usersPerRow);
	},
	_calculateTotalHeight: function(rowsOnPage){
		var singleElementHeight = this._getSingleElementHeight();
		return rowsOnPage * singleElementHeight;
	}
});

module.exports = UsersList;