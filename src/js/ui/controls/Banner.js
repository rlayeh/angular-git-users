var React = require('React');
var lang = require('../../lang/en-EN');

var Banner = React.createClass({
	render: function(){
		return <div className={'baner'}>
					<div className={'middle'}>
						<div className={'main'}>{lang.baner.gitHub}</div>
						<div className={'sub'}>{lang.baner.community}</div>
					</div>
			   </div>;
	}
});

module.exports = Banner;