var React = require('React');
var lang = require('../../lang/en-EN');
var RepositoriesList = require('./RepositoriesList');

var UserDetails = React.createClass({

	render: function(){
		var user = this.props.userData;

		return <div className={'user-details'} >
					{this._avatar(user)}
					{this._userDetails(user)}
					{this._repositoriesSection(this._getRepositories(user))}
				</div>
	},
	_repositoriesSection: function(repositories){
		return <RepositoriesList repositories={repositories} />;
	},
	_getRepositories: function(user){
		return user.getRepositories().map((function(repositoryId){
			return this._getRepositoryById(repositoryId);
		}).bind(this));
	},
	_getRepositoryById: function(repositoryId){
		if(this.props.getRepositoryCallback){
			return this.props.getRepositoryCallback(repositoryId);
		}
		return repositoryId;
	},
	_avatar: function(user){
		var avatarUrl = user.get('avatar_url');
		return <div className={'image-wrapper'}>
						<img src={avatarUrl} />
					</div>
	},
	_userDetails: function(user){
		var listItems = this._createDetailsRows(user);

		return <ul className={'user-details-list'}>{listItems}</ul>
	},
	_createDetailsRows: function(user){
		var entries = lang.user.details;
		var listItems = [];
		for(var entry in entries){
			var label = entries[entry];
			var value = user.get(entry);
			if(value){
				listItems.push(this._createDetailsRow(value, label));
			}
		}

		return listItems;
	},
	_createDetailsRow: function(value, label){
		return (<li key={value}>
					<span className={'label'} >{label+":"}</span>
					<span>{this._createValue(value)}</span>
				</li>);
	},
	_createValue: function(value){
		if(typeof value === 'string' && value.match("https?://")){
			return <a href={value}>{value}</a>
		}

		return value;
	}
});

module.exports = UserDetails;