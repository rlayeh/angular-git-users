var React = require('React');
var lang = require('../../lang/en-EN');
var ActionDispatcher = require('../ActionDispatcher');

var RepositoriesList = React.createClass({
	render: function(){
		var repositoriesListItems = this._createRepositoriesList();

		return <div>
					<span>{lang.reposList.repos}</span>
					<ul className={'repositiories-list'} >
						{repositoriesListItems}
					</ul>
				</div>
	},
	_createRepositoriesList: function(){
		var repositories = this.props.repositories || [];
		
		return repositories.map((function(repository){
			return this._createRepositoryEntry(repository);
		}).bind(this));
	},
	_createRepositoryEntry: function(repository){
		return <li key={repository.name}>
					<a href={repository.html_url} >{repository.name}</a>
				</li>;
	}
});

module.exports = RepositoriesList;