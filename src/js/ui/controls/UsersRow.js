var React = require('React');
var User = require('./User');

var UsersRow = React.createClass({
	render: function(){
		var rowStyle = {
			top: this.props.top
		};

		var users = this._getUsers();
		return <div style={rowStyle} className={'users-row'}>
					{users}
				</div>;
	},
	_getUsers: function(){
		var users = this.props.users;
		var singleElementSize = this.props.singleElementSize;
		return users.map(function(user){
			return <User key={user.get('login')} userData={user} size={singleElementSize} />
		});
	}
});

module.exports = UsersRow;