var React = require('React');
var lang = require('../../lang/en-EN');
var ActionDispatcher = require('../ActionDispatcher');

var User = React.createClass({
	render: function(){
		var userName = this._getUserProperty('login');
		var profileImageUrl = this._getUserProperty('avatar_url');
		var style = {
			width: this.props.size.width,
			height: this.props.size.height
		}

		return <div  onClick={this._showDetails} style={style} className={'user'}>
					<div className={'image-wrapper'}>
						<img src={profileImageUrl} />
					</div>
					<div className={"user-info"} >
						<div className={'login'}>{userName}</div>
						<div className={'additional-info'}>{this._totalContributionsLabel()}</div>
					</div>
				</div>;
	},
	_getUserData: function(){
		return this.props.userData;
	},
	_getUserProperty: function(propertyName){
		var userData = this._getUserData();
		if(userData){
			return userData.get(propertyName);
		}
		return null;
	},
	_showDetails: function(){
		ActionDispatcher.trigger('details', this._getUserData());
	},
	_totalContributionsLabel: function(){
		return lang.user.totalContributions + ': ' + this._getUserData().get('contributions');
	}
});

module.exports = User;