var React = require('React');
var lang = require('../lang/en-EN');
var ActionDispatcher = require('./ActionDispatcher');
var UserDetails = require('./controls/UserDetails'); 
var Banner = require('./controls/Banner');

var Details = React.createClass({
	render: function(){
		var user = this.props.user;
		return <div className={'details'}>
					<Banner />
					<div className={'navigation-link'} >
						<span onClick={this._navigateBack} >{lang.details.navigateBack}</span>
					</div>
					<UserDetails getRepositoryCallback={this.props.getRepositoryCallback} userData={user} />
				</div>;
	},
	_navigateBack: function(){
		ActionDispatcher.trigger('mainPage');
	}
});

module.exports = Details;