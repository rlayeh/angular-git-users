var React = require('React');
var GitHubUsersStore = require('../core/GitHub/stores/GitHubUsersStore');
var GitHubRepositoriesStore = require('../core/GitHub/stores/GitHubRepositoriesStore');
var FilterExpression = require('../core/FilterExpression');
var comparisonOperators = require('../core/ComparisonOperators')
var Loader = require('./Loader');
var MainPage = require('./MainPage');
var Details = require('./Details');

var ActionDispatcher = require('./ActionDispatcher');

var pages = {
	mainPage: 'mainPage',
	details: 'details',
	loader: 'loader'
}

var App = React.createClass({
	getInitialState: function(){
		var userStoreId = 'GitHubUsersStore';
		var repoStoreId = 'GitHubRepoStore';

		var filter = new FilterExpression()
						.field('contributions')
						.value(0)
						.operator(comparisonOperators.greaterThan);

		var usersStore = new GitHubUsersStore()
						.onUsersLoaded(this._contributorsLoaded)
						.onLoadFinished(this._usersLoaded)
						.onLoadFailed(this._renderError)
						.id(userStoreId)
						.filter(filter);

		var reposStore = new GitHubRepositoriesStore()
						.onLoadFinished(this._repositoriesLoaded)
						.onLoadFailed(this._renderError)
						.id(repoStoreId);

		return {
			usersStore: usersStore,
			reposStore: reposStore,
			loaded: false,
			usersLoaded: false,
			repositoryUserName: 'angular',
			currentPage: pages.mainPage,
			pageProps: {}
		}
	},

	componentWillMount: function(){
		var authToken = this.props.authToken;
		var repositoryUserName = this.state.repositoryUserName;

 		this.state.usersStore.authenticationToken(authToken);

		this.state.reposStore.authenticationToken(authToken)
							 .load(repositoryUserName);

		ActionDispatcher.on('details', this._gotToDetails);
		ActionDispatcher.on('mainPage', this._goToMainPage);
		ActionDispatcher.on('filter', this._filter);
	},

	render: function(){
		return this.state.loaded ? this._renderPage() : this._renderLoader();
	},
	_renderError: function(){
		//TODO
	},
	_renderLoader: function(){
		return <Loader loaded={this.state.loaded} />;
	},
	_renderPage: function(){
		return this._getPage(this.state.currentPage);
	},
	_renderMain: function(){
		var getTotalCountCallback = (function(){
			return this.state.usersStore.getTotalCount();
		}).bind(this);

		var getByIndexCallback = (function(index){
			return this.state.usersStore.getByIndex(index);
		}).bind(this);

		var filter = this.state.usersStore.getFilter();
		var loaded = this.state.usersLoaded;
		
		return <MainPage loaded={loaded} filter={filter} getTotalCountCallback={getTotalCountCallback} getByIndexCallback={getByIndexCallback} />;
	},
	_renderDetails: function(){
		var user = this.state.pageProps[pages.details];
		var getRepositoryCallback = (function(repositoryId){
			return this.state.reposStore.getRepositoryByName(repositoryId);
		}).bind(this);

		return <Details getRepositoryCallback={getRepositoryCallback} user={user} />;
	},
	_repositoriesLoaded: function(repoStore){
		this.state.usersStore.load(this.state.reposStore);
	},
	_contributorsLoaded: function(){
		this.setState({
			loaded: true
		});
	},
	_usersLoaded: function(){
		this.setState({
			loaded: true,
			usersLoaded: true
		});
	},
	_gotToDetails: function(user){
		var newPageProps = this.state.pageProps;
		newPageProps[pages.details] = user;

		this.setState({
			pageProps: newPageProps,
			currentPage: pages.details
		});
	},
	_goToMainPage: function(){
		this.setState({
			currentPage: pages.mainPage
		});
	},
	_filter: function(filter){
		this.state.usersStore.filter(filter);
		this.forceUpdate();
	},
	_getPage: function(page){
		if(page === pages.details){
			return this._renderDetails();
		}
		if(page === pages.loader){
			return this._renderLoader();
		}

		return this._renderMain();
	}
});

module.exports = App;