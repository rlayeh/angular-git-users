var ActionDispatcher = function(){
	this._callbacks = {};
}

ActionDispatcher.prototype = {
	on: function(actionName, callback){
		if(!this._callbacks[actionName]){
			this._callbacks[actionName] = [];
		}
		this._callbacks[actionName].push(callback);
	},
	trigger: function(actionName){
		var args = Array.prototype.slice.call(arguments, 1);

		this._trigger(actionName, args);
	},
	_trigger: function(actionName, attr){
		if(this._callbacks[actionName]){
			this._triggerCallbacks(this._callbacks[actionName], attr);
		}
	},
	_triggerCallbacks: function(callbacksArray, attr){
		for (var i = 0; i < callbacksArray.length; i++) {
			callbacksArray[i].apply(null, attr);
		}
	}
}

module.exports = new ActionDispatcher();