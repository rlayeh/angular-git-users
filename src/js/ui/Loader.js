var React = require('React');
var LoaderBar = require('./controls/LoaderBar');
var Banner = require('./controls/Banner');

var Loader = React.createClass({
	render: function(){
		return <div className={'loader'}>
					<Banner />
					<div className={'loader-div'}>
						<LoaderBar />
					</div>
				</div>;
	}
});

module.exports = Loader;