var HttpRequest = require('../../src/js/net/HttpRequest');
var nanoajax = require('nanoajax');

describe('HttpRequest', function(){

	it('should be createable',function(){
		expect(function(){
			new HttpRequest();
		}).not.toThrow();
	})

	it('should throw an error if path is not specified', function(){
		var request = new HttpRequest();

		expect(request.execute.bind(request)).toThrow();
	});

	it('should not throw an error if path is not specified', function(){
		spyOn(nanoajax,'ajax');

		var request = new HttpRequest('test');
		expect(request.execute.bind(request)).not.toThrow();		
	});

	describe('execute', function(){
		it('should execute call for specified url', function(){
			spyOn(nanoajax,'ajax');

			var request = new HttpRequest('testUrl');
			request.execute();
			expect(nanoajax.ajax).toHaveBeenCalled();
			expect(nanoajax.ajax.calls.count()).toBe(1);
			var call = nanoajax.ajax.calls.argsFor(0);

			var config = call[0];
			expect(config.url).toBe('testUrl');
		});

		it('should execute call with specified request method', function(){
			spyOn(nanoajax,'ajax');

			var request = new HttpRequest('testUrl')
								.method('POST');

			request.execute();
			expect(nanoajax.ajax).toHaveBeenCalled();
			expect(nanoajax.ajax.calls.count()).toBe(1);
			var call = nanoajax.ajax.calls.argsFor(0);

			var config = call[0];
			expect(config.method).toBe('POST');
		});

		it('should execute call with specified body', function(){
			spyOn(nanoajax,'ajax');
			var bodyObj = {
				test:'test1',
				test2:'test3'
			};

			var request = new HttpRequest('testUrl')
								.body(bodyObj);

			request.execute();
			expect(nanoajax.ajax).toHaveBeenCalled();
			expect(nanoajax.ajax.calls.count()).toBe(1);
			var call = nanoajax.ajax.calls.argsFor(0);

			var config = call[0];
			expect(config.body).toBe('test=test1&test2=test3');
		});

		it('should execute call with specified headers', function(){
			spyOn(nanoajax,'ajax');

			var headerObj = {}
			var request = new HttpRequest('testUrl')
								.headers(headerObj);

			request.execute();
			expect(nanoajax.ajax).toHaveBeenCalled();
			expect(nanoajax.ajax.calls.count()).toBe(1);
			var call = nanoajax.ajax.calls.argsFor(0);

			var config = call[0];
			expect(config.headers).toBe(headerObj);
		});
	});

	describe('success', function(){
		it('should not crash if onSuccess is not defined', function(){
			spyOn(nanoajax,'ajax').and.callFake(function(config, callback){

				callback(200, '');
			});

			var onSuccess = jasmine.createSpy('onSuccess');

			var request = new HttpRequest('testUrl')
								.onSuccess(onSuccess);

			expect(request.execute.bind(request)).not.toThrow();
		});

		it('should trigger onSuccess', function(){
			spyOn(nanoajax,'ajax').and.callFake(function(config, callback){

				callback(200, '');
			});

			var onSuccess = jasmine.createSpy('onSuccess');

			var request = new HttpRequest('testUrl')
								.onSuccess(onSuccess);

			request.execute();
			expect(onSuccess).toHaveBeenCalled();
		});

		it('should not trigger onSuccess if response code is lower than 200', function(){
			spyOn(nanoajax,'ajax').and.callFake(function(config, callback){
				callback(100, '');
			});

			var onSuccess = jasmine.createSpy('onSuccess');

			var request = new HttpRequest('testUrl')
								.onSuccess(onSuccess);

			request.execute();
			expect(onSuccess).not.toHaveBeenCalled();
		});

		it('should not trigger onSuccess if response code is higher than 299', function(){
			spyOn(nanoajax,'ajax').and.callFake(function(config, callback){
				callback(300, '');
			});

			var onSuccess = jasmine.createSpy('onSuccess');

			var request = new HttpRequest('testUrl')
								.onSuccess(onSuccess);

			request.execute();
			expect(onSuccess).not.toHaveBeenCalled();
		});

		it('should pass result as string', function(){;
			var responseText = "{responseText:\"responsetest\"}";
			spyOn(nanoajax,'ajax').and.callFake(function(config, callback){
				callback(200, responseText);
			});

			var onSuccess = jasmine.createSpy('onSuccess');

			var request = new HttpRequest('testUrl')
								.onSuccess(onSuccess);

			request.execute();
			expect(onSuccess).toHaveBeenCalledWith(responseText);
		});

		it('should pass result as an object', function(){;
			var responseText = "{\"responseText\":\"responsetest\"}";

			spyOn(nanoajax,'ajax').and.callFake(function(config, callback){
				callback(200, responseText);
			});

			var onSuccess = jasmine.createSpy('onSuccess');

			var request = new HttpRequest('testUrl')
								.responseAsObject(true)
								.onSuccess(onSuccess);

			request.execute();
			expect(onSuccess).toHaveBeenCalled();
			expect(onSuccess.calls.count()).toBe(1);

			var call = onSuccess.calls.argsFor(0);
			var response = call[0];

			expect(response).toEqual({responseText:"responsetest"});
		});
	});

	describe('failed', function(){
		it('should not crash if onFailed is not defined', function(){
			spyOn(nanoajax,'ajax').and.callFake(function(config, callback){

				callback(300, '');
			});

			var onFailed = jasmine.createSpy('onFailed');

			var request = new HttpRequest('testUrl')
								.onFailed(onFailed);

			expect(request.execute.bind(request)).not.toThrow();
		});

		it('should trigger onFailed if response code is higher than 299', function(){
			spyOn(nanoajax,'ajax').and.callFake(function(config, callback){
				callback(300, '');
			});

			var onFailed = jasmine.createSpy('onFailed');

			var request = new HttpRequest('testUrl')
								.onFailed(onFailed);

			request.execute();
			expect(onFailed).toHaveBeenCalled();
		});

		it('should  trigger onFailed if response code is lower than 200', function(){
			spyOn(nanoajax,'ajax').and.callFake(function(config, callback){
				callback(100, '');
			});

			var onFailed = jasmine.createSpy('onFailed');

			var request = new HttpRequest('testUrl')
								.onFailed(onFailed);

			request.execute();
			expect(onFailed).toHaveBeenCalled();
		});

		it('should not trigger onFailed if response code is between 200 and 299', function(){
			spyOn(nanoajax,'ajax').and.callFake(function(config, callback){
				callback(200, '');
			});

			var onFailed = jasmine.createSpy('onFailed');

			var request = new HttpRequest('testUrl')
								.onFailed(onFailed);

			request.execute();
			expect(onFailed).not.toHaveBeenCalled();
		});

		it('should pass: response code, response and request', function(){
			var responseText = "test";
			var requestObj = {};
			var responseCode = 300;
			spyOn(nanoajax,'ajax').and.callFake(function(config, callback){
				callback(responseCode, responseText, requestObj);
			});

			var onFailed = jasmine.createSpy('onFailed');

			var request = new HttpRequest('testUrl')
								.onFailed(onFailed);

			request.execute();
			expect(onFailed).toHaveBeenCalledWith(responseCode, responseText, requestObj);
		});
	});
}); 