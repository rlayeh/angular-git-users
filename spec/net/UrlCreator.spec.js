var UrlCreator = require('../../src/js/net/UrlCreator')

describe('UrlCreator', function(){
	it('should be createable',function(){
		expect(function(){
			new UrlCreator();
		}).not.toThrow();
	})

	it('should create empty path', function(){
		var creator = new UrlCreator();
		expect(creator.create()).toBe('');
	});

	it('should create path from base path', function(){
		var creator = new UrlCreator('testPath');
		expect(creator.create()).toBe('testPath');
	});

	it('should create path for specified paths', function(){
		var creator = new UrlCreator('testPath')
							.append('a')
							.append('b')
							.append('c');

		expect(creator.create()).toBe('testPath/a/b/c');
	});

	it('should create path with a different separator', function(){
		var creator = new UrlCreator('testPath')
							.append('a')
							.append('b')
							.append('c')
							.separator(':');

		expect(creator.create()).toBe('testPath:a:b:c');
	});
}); 