var GitHubApiRequest = require('../../src/js/net/GitHubApiRequest');
var nanoajax = require('nanoajax');

var GITHUB_URL = "https://api.github.com";

describe('GitHubApiRequest', function(){
	it('should be createable',function(){
		expect(function(){
			new GitHubApiRequest();
		}).not.toThrow();
	})

	describe('request creation', function(){
		it('should contain base api url', function(){
			spyOn(nanoajax,'ajax');
			new GitHubApiRequest()
					.getRequest()
					.execute();

			var call = nanoajax.ajax.calls.argsFor(0);

			var config = call[0];
			expect(config.url).toBe(GITHUB_URL);
		});

		it('should get request without headers', function(){
			spyOn(nanoajax,'ajax');
			new GitHubApiRequest()
					.getRequest()
					.execute();

			var call = nanoajax.ajax.calls.argsFor(0);

			var config = call[0];
			expect(config.headers).toBeNull();
		});

		it('should get request with authentication header', function(){
			spyOn(nanoajax,'ajax');
			var token = 'testToken123';

			new GitHubApiRequest()
					.authenticationToken(token)
					.getRequest()
					.execute();
			var call = nanoajax.ajax.calls.argsFor(0);

			var config = call[0];
			expect(config.headers).toEqual({Authorization: 'token '+ token});
		});

		it('should execute onSuccess after successful request' ,function(){
			spyOn(nanoajax,'ajax').and.callFake(function(config, callback){
				callback(200, '{"test": "test"}');
			});

			var onSuccess = jasmine.createSpy('onSuccess');

			new GitHubApiRequest()
					.onSuccess(onSuccess)
					.getRequest()
					.execute();


			expect(onSuccess).toHaveBeenCalled();
		});

		it('should pass parsed JSON to onSuccess' ,function(){
			spyOn(nanoajax,'ajax').and.callFake(function(config, callback){
				callback(200, '{"test": "test"}');
			});

			var onSuccess = jasmine.createSpy('onSuccess');

			new GitHubApiRequest()
					.onSuccess(onSuccess)
					.getRequest()
					.execute();

			var call = onSuccess.calls.argsFor(0);
			var response = call[0];

			expect(response).toEqual({test: "test"});
		});

		it('should execute onFailed after failed request', function(){
			spyOn(nanoajax,'ajax').and.callFake(function(config, callback){
				callback(300, 'fail', {});
			});

			var onFailed = jasmine.createSpy('onFailed');

			new GitHubApiRequest()
					.onFailed(onFailed)
					.getRequest()
					.execute();

			expect(onFailed).toHaveBeenCalled();
		});

		it('should pass response code, reqsponseText and request onFailed' ,function(){
			var request = {};
			var responseText = 'testText';
			var responseCode = 300;

			spyOn(nanoajax,'ajax').and.callFake(function(config, callback){
				callback(responseCode, responseText, request);
			});

			var onFailed = jasmine.createSpy('onFailed');

			new GitHubApiRequest()
					.onFailed(onFailed)
					.getRequest()
					.execute();

			expect(onFailed).toHaveBeenCalledWith(responseCode, responseText, request);
		});
	});

	describe('execution', function(){
		beforeEach(function(){
			spyOn(nanoajax,'ajax');
		});

		describe('getRatesLimit', function(){
			it('should execute request', function(){
				 new GitHubApiRequest()
						.getRateLimit();

				expect(nanoajax.ajax).toHaveBeenCalled();
			});

			it('should obtain a request from getRequest method', function(){
				var request = new GitHubApiRequest();
				spyOn(request, 'getRequest').and.callThrough();

				request.getRateLimit();
				expect(request.getRequest).toHaveBeenCalled();
			});

			it('should execute request with a correct path', function(){
				new GitHubApiRequest()
					.getRateLimit();

				var calls = nanoajax.ajax.calls.argsFor(0);
				var config = calls[0];

				expect(config.url).toBe(GITHUB_URL + '/rate_limit');
			});
		});

		describe('getContributors', function(){
			it('should throw an error if repository name is not specified', function(){
				var request = new GitHubApiRequest();

				expect(request.getContributors.bind(request)).toThrow();
			});

			it('should throw an error if repository name is not a full name', function(){
				var request = new GitHubApiRequest();

				expect(request.getContributors.bind(request, 'test')).toThrow();
			});

			it('should not throw an error if full repository name was specified', function(){
				var request = new GitHubApiRequest();

				expect(request.getContributors.bind(request, 'test/test')).not.toThrow();
			});

			it('should execute', function(){
				new GitHubApiRequest()
						.getContributors('test/test');

				expect(nanoajax.ajax).toHaveBeenCalled();
			});

			it('should obtain a request from getRequest method', function(){
				var request = new GitHubApiRequest();
				spyOn(request, 'getRequest').and.callThrough();

				request.getContributors('test/test');
				expect(request.getRequest).toHaveBeenCalled();
			});

			it('should execute request with a correct path', function(){
				new GitHubApiRequest()
					.getContributors('test/test');

				var calls = nanoajax.ajax.calls.argsFor(0);
				var config = calls[0];

				expect(config.url).toBe(GITHUB_URL + '/repos/test/test/contributors');
			});
		});

		describe('getUser', function(){
			it('should throw an error if user name is not specified', function(){
				var request = new GitHubApiRequest();

				expect(request.getUser.bind(request)).toThrow();
			});

			it('should not throw an error if user name is specified', function(){
				var request = new GitHubApiRequest();

				expect(request.getUser.bind(request, 'testUser')).not.toThrow();
			});

			it('should execute', function(){
				new GitHubApiRequest()
						.getUser('test');

				expect(nanoajax.ajax).toHaveBeenCalled();
			});

			it('should obtain a request from getRequest method', function(){
				var request = new GitHubApiRequest();
				spyOn(request, 'getRequest').and.callThrough();

				request.getUser('test');
				expect(request.getRequest).toHaveBeenCalled();
			});

			it('should execute request with a correct path', function(){
				new GitHubApiRequest()
					.getUser('test');

				var calls = nanoajax.ajax.calls.argsFor(0);
				var config = calls[0];

				expect(config.url).toBe(GITHUB_URL + '/users/test');
			});
		});

		describe('getRepositories', function(){
			it('should throw an error if user name is not specified', function(){
				var request = new GitHubApiRequest();

				expect(request.getRepositories.bind(request)).toThrow();
			});

			it('should not throw an error if user name is specified', function(){
				var request = new GitHubApiRequest();

				expect(request.getRepositories.bind(request, 'testUser')).not.toThrow();
			});

			it('should execute', function(){
				new GitHubApiRequest()
						.getRepositories('test');

				expect(nanoajax.ajax).toHaveBeenCalled();
			});

			it('should obtain a request from getRequest method', function(){
				var request = new GitHubApiRequest();
				spyOn(request, 'getRequest').and.callThrough();

				request.getRepositories('test');
				expect(request.getRequest).toHaveBeenCalled();
			});

			it('should execute request with a correct path', function(){
				new GitHubApiRequest()
					.getRepositories('test');

				var calls = nanoajax.ajax.calls.argsFor(0);
				var config = calls[0];

				expect(config.url).toBe(GITHUB_URL + '/users/test/repos');
			});
		});
	});

}); 